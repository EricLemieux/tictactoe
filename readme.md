Tic-Tac-Toe
----------

This is a simple tic tac toe game for an AI class using minimax for the computer player's decisions.

A version is available online at http://lemieuxdev.com/TicTacToe/