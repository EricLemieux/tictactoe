/*
This is the main file for the tic tac toe game
*/

var canvas;
var ctx;

var playersTurn = true;
var gameboard = [
	[0,0,0],
	[0,0,0],
	[0,0,0]];

var playerColour = "rgb(255,0,0)";
var aiColour = "rgb(0,0,255)";

function StartGame(){
	canvas = document.getElementById('mycanvas');
	ctx = canvas.getContext('2d');

	ClearBoard();
}

function ClearBoard(){
	if(confirm("Do you want to be the red team?") ==  true){
		playerColour = "rgb(255,0,0)";
		aiColour = "rgb(0,0,255)";
	}else{
		playerColour = "rgb(0,0,255)";
		aiColour = "rgb(255,0,0)";
	}

	gameboard = [
	[0,0,0],
	[0,0,0],
	[0,0,0]];

	playersTurn = true;

	ConstructNodeTree();

	DrawBoard();
}

function VictoryCheck(){
	UpdateActiveNode();

	var foo = GameOverCheck(gameboard);

	if(foo == -1){
		//game not over yet
		return false;
	}else{
		if(foo == 0){
			alert("its a tie!")
			ClearBoard();
		}else if(foo == 1){
			alert("Player Wins!");
		}else if(foo == 2){
			alert("AI Wins!");
		}

		ClearBoard();
		return true;
	}
}

function GameOverCheck(board){
	//Check to see if anyone won.
	for(var i = 0; i < board.length; ++i){
		if(board[i][1] != 0 && board[i][1] == board[i][0] && board[i][1] == board[i][2]){
			return board[i][1];
		}

		if(board[1][i] != 0 && board[1][i] == board[0][i] && board[1][i] == board[2][i]){
			return board[1][i];
		}
	}

	//Check the diagonals
	if((board[1][1] != 0 && board[1][1] == board[0][0] && board[1][1] == board[2][2]) ||
		(board[1][1] != 0 && board[1][1] == board[0][2] && board[1][1] == board[2][0]) ){
		return board[1][1];
	}

	//no one won but the board might be full
	var full = true;
	for(var i = 0; i < board.length; ++i){
		for(var j = 0; j < board[i].length; ++j){
			if(board[i][j] == 0){
				full = false;
			}
		}
	}
	if(full){
		return 0;
	}

	//No one won and game not over
	return -1;
}

function GetMousePos(evt){
	var rect = canvas.getBoundingClientRect();
	return{
		x: evt.clientX - rect.left,
		y: evt.clientY - rect.top
	};

}

function PlayerInput(evt){
	//Only accept input if it is the players turn
	if(playersTurn){
		//Block the player until its their turn again
		playersTurn = false;

		//Process input
		var mousePos = GetMousePos(evt);
		var i = Math.floor(mousePos.x/100);
		var j = Math.floor(mousePos.y/100);
		if(gameboard[j][i] == 0){
			gameboard[j][i] = 1;
		}else{
			playersTurn = true;
			return;
		}

		//Draw the board based on the players input
		DrawBoard();

		//Check to see if the player won
		if(!VictoryCheck()){
			//Process the AI's turn
			AIPick();

			//Draw the board again
			DrawBoard();

			//Check to see if the AI won
			VictoryCheck();

			//Stop blocking the player now
			playersTurn = true;
		}
	}
}

function DrawBoard(){
	//Draw the grid
	ctx.fillStyle = "rgb(0,0,0)";
	ctx.fillRect(92, 0, 5, 300);
	ctx.fillRect(192, 0, 5, 300);
	ctx.fillRect(0, 92, 300, 5);
	ctx.fillRect(0, 192, 300, 5);

	//Draw the players selections
	for(var i = 0; i < gameboard.length; ++i){
		for(var j = 0; j < gameboard[i].length; ++j){
			if(gameboard[i][j] == 0){
				ctx.fillStyle = "rgb(255,255,255)";
			}else if(gameboard[i][j] == 1){
				ctx.fillStyle = playerColour;
			}else if(gameboard[i][j] == 2){
				ctx.fillStyle = aiColour;
			}

			ctx.fillRect(j * 100, i * 100, 90, 90);
		}
	}
}

function CompareGameboards(board1, board2){
	for(var i = 0; i < board1.length; ++i){
		for(var j = 0; j < board1[i].length; ++j){
			if(board1[i][j] != board2[i][j]){
				return false;
			}
		}
	}
	return true;
}