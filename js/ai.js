function minimax(node, maximizingPlayer){
	if(node.depth == 0 || node.children.length == 0){
		return node.value;
	}

	if(maximizingPlayer){
		var bestValue = -9999999999999999;
		for(var i = 0; i < node.children.length; ++i){
			var value = minimax(node.children[i], false);
			bestValue = Math.max(bestValue, value);
		}
		return bestValue;
	}else{
		var bestValue = 9999999999999999;
		for(var i = 0; i < node.children.length; ++i){
			var value = minimax(node.children[i], true);
			bestValue = Math.min(bestValue, value);
		}
		return bestValue;
	}
}

function negamax(node, v){
	if(node.depth == 0 || node.children.length == 0){
		return v * node.value;
	}

	var bestValue = -99999999999;

	for(var i = 0; i < node.children.length; ++i){
		var value = -negamax(node.children[i], -v);
		bestValue = Math.max(bestValue, value);
	}
	return bestValue;
}

function negamaxAB(node, alpha, beta, v){
	if(node.depth == 0 || node.children.length == 0){
		return v * node.value;
	}

	var bestValue = -99999999999;

	for(var i = 0; i < node.children.length; ++i){
		var value = -negamaxAB(node.children[i], -beta, -alpha, -v);
		bestValue = Math.max(bestValue, value);
		alpha = Math.max(alpha, value);
		if(alpha >= beta){
			break;
		}
	}
	return bestValue;
}

function AIPick(){
	var picks = [];

	var bestValue = 9999999;
	var newNode = new Node();
	console.log("new shit");
	for(var i = 0; i < activeNode.children.length; ++i){
		var value = negamaxAB(activeNode.children[i], -999, 999, 1);
		console.log("value = "+value);
		if(value < bestValue){
			console.log("best value");
			newNode = activeNode.children[i];
			bestValue = value;
			picks = [i];
		}else if(value == bestValue){
			picks.push(i);
		}
	}
	console.log("\n");

	newNode = activeNode.children[picks[RandInt(0,picks.length-1)]];

	gameboard = newNode.boardState;
	activeNode = newNode;
}

//If there is no good choise pick a random spot
function AIRandom(){
	var aiPicked = false;
	while(!aiPicked){
		var i = RandInt(0,2);
		var j = RandInt(0,2);

		if(gameboard[j][i] == 0){
			gameboard[j][i] = 2;
			aiPicked = true;
		}
	}
}

function RandInt(min, max){
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function GenerateMinMaxTree(){
	//Find the number of nodes we need to make
	var nodeCount = activeNode.children.length;
	var rowNodes = activeNode.children.length;
	while(rowNodes != 1){
		rowNodes = Math.ceil(rowNodes/2);
		nodeCount += rowNodes;
	}

	//Generate the number of nodes needed
	var arr = [];
	for (var i = 0; i < nodeCount; ++i) {
		var n = new Node(0,[]);
		arr.push(n);
	};

	//Set the depth
	arr[0].depth = Math.ceil(Math.log2(activeNode.children.length));

	//link the nodes and assign values
	var assigning = 1;
	for(var i = 0; i < nodeCount; ++i){
		if(assigning < nodeCount-1){
			console.log("["+i+"] = ["+assigning+"]");
			arr[assigning].depth = arr[i].depth - 1;
			arr[i].children.push(arr[assigning]);
			assigning++;
			//Crappy workaround for now
			if(!(activeNode.children.length == 6 && i == 2)){
				console.log("["+i+"] = ["+assigning+"]");
				arr[assigning].depth = arr[i].depth - 1;
				arr[i].children.push(arr[assigning]);
				assigning++;
			}
		}else{
			arr[i].value = activeNode.children[nodeCount - i - 1].value;
		}
	}

	console.log(arr[0]);

	return arr[0];
}