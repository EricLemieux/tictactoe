var origin;
var activeNode;

var firstRun = true;
var treeJson = "";

var outcomeValues = [0,1,-1];

function Node(val, child, board, level, depth){
	this.value = val;
	this.children = child;
	this.boardState = board;
	this.level = level;
	this.depth = depth;
}

//This is where we make the tree that the ai is going to use to make decisions
function ConstructNodeTree(){
	if(firstRun){
		origin = new Node(0,[],[[0,0,0],[0,0,0],[0,0,0]], 0, 8);
		origin = GenerateNode(origin);
		treeJson = JSON.stringify(origin);
		firstRun = false;
	}else{
		origin = JSON.parse(treeJson);
	}

	activeNode = origin;
}

//Recursivly generate nodes
function GenerateNode(parent){
	for(var i = 0; i < gameboard.length; ++i){
		for(var j = 0; j < gameboard[i].length; ++j){
			if(parent.boardState[j][i] == 0){
				var level = parent.level+1;
				var depth = parent.depth-1;
				var b = [[0,0,0],[0,0,0],[0,0,0]];
				b[0] = parent.boardState[0].slice();
				b[1] = parent.boardState[1].slice();
				b[2] = parent.boardState[2].slice();

				var newNode = new Node(0,[], b, level, depth);

				newNode.boardState[j][i] = 2-(newNode.level%2);		

				var check = GameOverCheck(newNode.boardState);
				if(check != -1){
					newNode.value = (outcomeValues[check]) * (2 + (4*depth));
				}else{
					newNode = GenerateNode(newNode);
				}				

				parent.children.push(newNode);
			}
		}
	}
	

	return parent;
}

function UpdateActiveNode(){
	for(var i = 0; i < activeNode.children.length; ++i){
		if(CompareGameboards(activeNode.children[i].boardState,gameboard)){
			activeNode = activeNode.children[i];
		}
	}
}